## Introduction: Data, a matter in itself?

Digital technology and the internet have fundamentally
transformed the scale and speed of creating and sharing
data[^Toby]. The ability to reproduce exact copies of data
(whether image, text or video) and distribute them to
storage centres and screens around the entire globe
almost instantaneously is already having profound
societal effects. Although there are infrastructure
costs associated with the manufacture, purchase and
maintenance of the networks required to access and
distribute data, from the point of view of the user,
images, files and texts can be replicated at no
discernible cost to the operator or damage to the
original. In economic parlance, a resource that is
simultaneously available to different persons at quasi
no additional cost is called a "non-rival good"
[@Lessig2001a, 19-23], implying that it is not
necessary to 'fight' for such goods since they are
essentially free and 'un-consumable' -- although this
has not stopped many from trying to commoditise
immaterial objects into consumable products as the
ultimate in value-added goods[^music].

[^Toby]: Starting point of this part were some ideas
generated while working on the article with Toby C.
Wilkinson [see @Strupler2017reproducibility] . We are
grateful for his support t this
article.

[^music]: Perhaps the best known example being the
music industry, which has used anti-copying
technologies and strong intellectual property laws to
restrain 'free' music
sharing and sell digital recordings.

The technology enabling unlimited, near-free
reproduction of data or knowledge has demanded new
social strategies to contain or enhance its effects for
different stakeholders. It has been in the field of
'intellectual property', of 'licences' and 'rights',
that much of the debate around new technology has been
fought [@Lessig2001a; for an anthropological
perspective, see @Strathern1999ps].  Perhaps
unsurprisingly, the vanguard of new sharing systems and
principles were created in computing science, under the
rubric of Free Software[^FS] and represented in legal
terms by certain licences, such as GNU, BSD or MIT
[@FSF2016a], designed to enhance sharing and ensure the
freedom to run and/or modify a program (especially for
goals not previously envisaged or to fix bugs). But
the same discourse over 'intellectual property' in
computer code has spilled over into other forms of
cultural production, under the banner of Creative
Commons or similar licences.  Digital creation
workflows and distribution models, especially those
that harness the web, present new opportunities for
knowledge circulation. For example, in the case of
writing, texts have the potential to be highly
'dynamic', where content can be adapted and updated
quickly, not simply a static or locked facsimile of
printed page. The best-known although sometimes
contentious example of a collaborative writing is
[Wikipedia](https://wikipedia.com), a platform that
shows how it is possible to contribute, update, reuse,
expand or distribute knowledge as well as exemplifying
debate around authority and accuracy which 'openness'
creates. It is, of course, only one of many dynamic
systems already implemented in books, blogs, wikis,
webpages, etc [@Heller2014a].


[^FS]: We use the term 'Free Software' for convenience over terms like FLOSS
(Free/Libre and Open Source Software) but also because the term 'Free Software'
is older and focus more on the ideal and political aims, as 'Open Source' was
coined for the business world (http://opensource.org/history). Free Software
started in University Laboratories, was inspired by academic ideals of universal
education and freedom of research, and addresses technical and legal basis of
freedom: freedom of use, reuse, distribution, study, improvement and (personal)
control.

In academia, most authors do not expect to receive a
directly correlated financial reward for their
products, and in this sense research outputs could be
seen as "non-rival goods", especially in an social
environment where cooperation is discursively praised
[@Kelty2005b; @Suber2012a, 43-48]. Nonetheless,
individual researchers do hope for delayed return in
the sense of recognition, appointment and promotion;
and institutions hope for prestige and financial reward
in the form of student demand and grants; both sets of
motivations rarely in alignment with successful
cooperation and more often promoting competition and
secrecy. The effects of the 'digital revolution' have
thus been different and somewhat delayed. This inertia
has much more to do with the social environment than
available technologies [see @Wallis2013a; cf.
@Pfaffenberger1992a; @Scheliga2014a].  The conservative
nature of academic incentives has often failed to
reward new models of knowledge circulation
[@Fitzpatrick2011a].

However, Open Access (OA) publication, that is restriction-free
access and publication of outputs, has been gaining
momentum. Open Data (OD), the free access to data, is
gaining more attention too, especially since the
involvement of government for general policies in
Science [SNF open data] that are coupled with
initiatives specific for different field. Open Data
concentrates on an open-model of data management and
dissemination: access to raw measurements, recording
and dissemination of (meta-)documentation (methods of
collection, documenting meta-data) and appropriate
archiving [@Costa2013a; @OpenKnowledge2015a;
@Strupler2017reproducibility].


In the constellation of scientific disciplines,
Archaeology is sitting at a crossroad between
quantitative and qualitative analyses. It employs a
wide range of physico-chemical methods, from
remote-sensing to ancient DNA analysis as well as
current (or past) development in social science,
anthropology, philosophy or history.  Despite the
convincing call for a radical change in the mode of
dissemination of archaeological [e.g.  @Kansa2014a;
@SchloenSchloen2014bg], it has been variously perceived
and deploy. We
believe that the top-down approach driven by experts
may also impend the wider acceptance and implementation
of Open Data by not clearly demonstrating the benefits
for the producers as well as for the consumer of data.
Defined as "a piece of information", 'data' suggests
the idea of a neutral or objective entity. It is
unclear however where one should make a division
between 'data', 'knowledge' or 'ideas' and we believe
that definition of 'data' is always dependent on the
understanding of producer and consumer [@Kratz2014a].


