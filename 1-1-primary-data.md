### Data and data-set: Definition and Quantification

#### Primary vs secondary data

Scientists consume data in two forms: as the
primary data generated during research,
and as secondary data, which are derived from combining
primary data in specific ways to address a research
question. The concept of primary data is relatively
straightforward: these are the observable phenomena we
record during research, and which,
putting aside issues related to the subjectivity
inherent in identifying data, could
hypothetically be replicated by a second observer.
Secondary data, however, is a concept that can
encompasses two different ways in which primary data
are combined and interpreted. In some subfields in
Humanities secondary data is derived by the analyst in
order to analyze and interpret the primary data as a
routine part of data reporting [e.g., see discussions
in @Lyman2008; and  @Reitz2008 for its use in
zooarchaeolgoical data]. These "secondary" or "derived"
data are may be treated as primary data by data
consumers but since they are drawn from primary data
and are not directly observable or replicable by
another analyst, they are categorically different. In
the social sciences, "secondary data" refers to
data generated for a particular research objective, but
reused in another context to address another research
question [@Devine2003; @Hox2005]. In this
case the primary and secondary data are the same; it is
the context of their use that determines which its
categorization. These two different ways of
conceptualizing secondary data share an understanding
of secondary data as something derivative of primary
data, either as a necessary interpretive step as the
analyst, or as an external researcher reaping the
benefits of collective advances in the field. The
increased capacity to publish primary data digitally
has greatly increased access to datasets and
facilitated their reuse as secondary data.

#### Data-set: raw vs processed

A widespread classification splits data-sets into
two categories: raw data-set  and processed data-set.
The first category refers to the data in their prime
state, as close as possible as their collection time.
This can refer to the data output from a sensor
(photo-sensor, magnetic-sensor, X-Ray sensor, chemical
sensor, …) or data produced by humans recording what
they see and measure (such as count of individual,
answer from a survey). The adjective "raw" indicates
that data generally need further work prior to an
analyse, such as correction for impossible/improbable
data (happening for example when a decimal fraction is
misplaced, i.e. 16.8m instead of 1.68m), or missing
values [@McIver2004]. In some cases, it is impossible to share or
publish raw data-set. This is specifically the case when
data contains sensitive or personal information, for
which only anonymised or aggregated data
may be made available. Geographical information are
also sensitive as it may allow direct identification
[@Lagoze2013a;@Strupler2017reproducibility].These raw-data
are published with some retractions, becoming
processed. Consequently "processed
data-set" means data that have been checked, verified,
and prepared for the analyses. These is usually the
kind of data-set that are published with articles
allowing other researcher to proof the analyse of the
article [@Marwick2016a]. Processed data have the advantage to having
been verified, corrected and checked for their
validity. There may be easier to reuse than processed
data. However, it may be more difficult to use a
processed data-set to ask new research question.

